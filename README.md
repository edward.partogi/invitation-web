
Proyek ini dibuat dengan tujuan awalnya adalah untuk memenuhi kebutuhan proyek kelompok kelas PMPL 2020/2021, Jurusan Ilmu Komputer, Universitas Indonesia.

Collaborator:
- Edward Partogi Gembira Abyatar
- Nur Nisrina

# Deskripsi Singkat Proyek
Proyek yang akan dibuat adalah website untuk menampilkan undangan pernikahan. Fitur yang akan dibuat kira-kira menyimpan hal-hal seperti peserta, alamat, dan sebagainya.
