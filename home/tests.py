from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest, response
from django.template.loader import render_to_string

from home.views import home_page  

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')  
        self.assertEqual(found.func, home_page)  

    def test_uses_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_returns_correct_html(self):
        response = self.client.get('/')

        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<html>'))
        self.assertIn('<title>Wedding Invitation</title>', html)
        self.assertTrue(html.strip().endswith('</html>'))

        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_contains_correct_h1(self):
        response = self.client.get('/')

        html = response.content.decode('utf8')
        self.assertIn('<h1>Bonny & Clyde</h1>', html)

        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_contains_correct_h4_date(self):
        response = self.client.get('/')

        html = response.content.decode('utf8')
        self.assertIn('<h4>Saturday, 9th January 2020</h4>', html)

        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_contains_correct_h4_place(self):
        response = self.client.get('/')

        html = response.content.decode('utf8')
        self.assertIn('<h4>Gelora Bung Karno, Jakarta</h4>', html)

        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_in_correct_bg_color(self):
        response = self.client.get('/')
        html = response.content.decode('utf8')

        self.assertIn("background-color: thistle ", html)