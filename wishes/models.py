from django.db import models
from django.core.exceptions import ValidationError

def validate_message(value):
    symbols = "!\"#$%&()*+-.,/:;<=>?@[\]^_`{|}~\n "
    if len(value) <= 1 and value in symbols:
        raise ValidationError(
            value+" cannot be the only value"
        )

def validate_sender(value):
    symbols = "!\"#$%&()*+-/:;<=>?@[\]^_`{|}~\n"
    for char in value:
        if char in symbols:
            raise ValidationError(
                char+" in '"+value+"' is not a valid name character"
            )

class Wish(models.Model):
    sender = models.TextField(validators=[validate_message, validate_sender])
    message = models.TextField(validators=[validate_message])