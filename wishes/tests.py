from django.test import TestCase
from django.core.exceptions import ValidationError
from wishes.models import Wish
from wishes.forms import WishForm

class WishesModelTest(TestCase):

    def test_saving_and_retrieving_wishes(self):
        first_wish = Wish()
        first_wish.sender = "Edward"
        first_wish.message = "Selamat menikmati hidup baru! :)"
        first_wish.save()

        second_wish = Wish()
        second_wish.sender = "Nur"
        second_wish.message = "Wish you both a happy life together!"
        second_wish.save()

        saved_items = Wish.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.message, 'Selamat menikmati hidup baru! :)')
        self.assertEqual(first_saved_item.sender, 'Edward')
        self.assertEqual(second_saved_item.message, 'Wish you both a happy life together!')
        self.assertEqual(second_saved_item.sender, 'Nur')

    def test_cannot_save_empty_wish_message(self):
        empty_wish = Wish(message="", sender="Abcde")
        with self.assertRaises(ValidationError):
            empty_wish.save()
            empty_wish.full_clean()

        symbols = "!\"#$%&()*+-.,/:;<=>?@[\]^_`{|}~\n "
        for symbol in symbols:
            single_char_wish = Wish(message=symbol, sender="Abcde")
            with self.assertRaises(ValidationError):
                single_char_wish.save()
                single_char_wish.full_clean()

    def test_cannot_save_invalid_wish_sender(self):
        empty_wish = Wish(message="Semoga baik!", sender="")
        with self.assertRaises(ValidationError):
            empty_wish.save()
            empty_wish.full_clean()

        symbols = "!\"#$%&()*+-.,/:;<=>?@[\]^_`{|}~\n "
        for symbol in symbols:
            if symbol not in ",. ":
                name_with_symbol_wish = Wish(message="Semoga baik!", sender="Sinta "+symbol)
                with self.assertRaises(ValidationError):
                    name_with_symbol_wish.save()
                    name_with_symbol_wish.full_clean()

            single_char_name_wish = Wish(message="Semoga baik!", sender=symbol)
            with self.assertRaises(ValidationError):
                single_char_name_wish.save()
                single_char_name_wish.full_clean()

class WishesFormTest(TestCase):
    def test_empty_wish_form(self):
        form = WishForm(data={})
        self.assertFalse(form.is_valid())

    def test_using_wish_form(self):
        form = WishForm(data={"sender":"Edward Partogi", "message":"A message!"})
        self.assertEqual(form.data["sender"], "Edward Partogi")
        self.assertEqual(form.data["message"], "A message!")
        self.assertTrue(form.is_valid())

class WishPageTest(TestCase):
    def test_wish_page_url(self):
        response = self.client.get('/wish/')
        self.assertEqual(response.status_code, 200)

    def test_wish_page_content(self):
        response = self.client.get('/wish/')
        self.assertTemplateUsed(response, 'wish.html')
        self.assertIn('Pesan dan Salam', response.content.decode())

    def test_wish_page_has_wishes(self):
        new_wish = Wish()
        new_wish.sender = "TestSender"
        new_wish.message = "Test Selamat menikmati hidup baru! :)"
        new_wish.save()

        second_wish = Wish()
        second_wish.sender = "Nur"
        second_wish.message = "Wish you both a happy life together!"
        second_wish.save()

        response = self.client.get('/wish/')
        self.assertIn('TestSender', response.content.decode())
        self.assertIn('Test Selamat menikmati hidup baru! :)', response.content.decode())

        self.assertIn('Nur', response.content.decode())
        self.assertIn('Wish you both a happy life together!', response.content.decode())
