from django import forms

from wishes.models import Wish

class WishForm(forms.ModelForm):

    class Meta:
        model = Wish
        fields = ["sender", "message"]
        widgets = {
            'sender': forms.fields.TextInput(attrs={
                'placeholder': 'Input your name',
                'class': 'form-control input-lg'
            }),
            'message': forms.fields.TextInput(attrs={
                'placeholder': 'Write your message',
                'class': 'form-control input-lg'
            })
        }

    # def clean_sender(self):
    #     sender = self.cleaned_data["sender"]
    #     if not sender:
    #         return sender

    #     symbols = "!\"#$%&()*+-/:;<=>?@[\]^_`{|}~\n"
    #     for char in sender:
    #         if char in symbols:
    #             self.add_error("sender", char+" in '"+sender+"' is not a valid name character")

    #     return sender