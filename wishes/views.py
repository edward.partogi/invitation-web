from django.shortcuts import render
from wishes.models import Wish

def wish_page(request):
    wishes = Wish.objects.all()

    return render(request, 'wish.html', {'wishes' : wishes})
